const assert = require('assert');
const app = require('../../src/app');

describe('\'kanji\' service', () => {
  it('registered the service', () => {
    const service = app.service('kanji');

    assert.ok(service, 'Registered the service');
  });
});
