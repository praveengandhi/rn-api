const assert = require('assert');
const app = require('../../src/app');

describe('\'wordGroups\' service', () => {
  it('registered the service', () => {
    const service = app.service('word-groups');

    assert.ok(service, 'Registered the service');
  });
});
