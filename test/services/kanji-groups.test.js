const assert = require('assert');
const app = require('../../src/app');

describe('\'kanji-groups\' service', () => {
  it('registered the service', () => {
    const service = app.service('kanji-groups');

    assert.ok(service, 'Registered the service');
  });
});
