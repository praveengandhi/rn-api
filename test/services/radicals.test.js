const assert = require('assert');
const app = require('../../src/app');

describe('\'radicals\' service', () => {
  it('registered the service', () => {
    const service = app.service('radicals');

    assert.ok(service, 'Registered the service');
  });
});
