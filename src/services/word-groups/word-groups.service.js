// Initializes the `wordGroups` service on path `/word-groups`
const createService = require('feathers-nedb');
const createModel = require('../../models/word-groups.model');
const hooks = require('./word-groups.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/word-groups', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('word-groups');

  service.hooks(hooks);
};
