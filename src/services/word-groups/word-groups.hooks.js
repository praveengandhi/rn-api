const { authenticate } = require('@feathersjs/authentication').hooks;
const authHooks = require('feathers-authentication-hooks');
module.exports = {
  before: {
    all: [ authenticate('jwt') ],
    find: [authHooks.restrictToOwner()],
    get: [authHooks.restrictToOwner()],
    create: [authHooks.associateCurrentUser()],
    update: [authHooks.associateCurrentUser({as:'updatedBy'})],
    patch: [authHooks.associateCurrentUser({as:'updatedBy'})],
    remove: [authHooks.restrictToOwner()]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
