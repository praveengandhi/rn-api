// Initializes the `kanji` service on path `/kanji`
const createService = require('feathers-nedb');
const createModel = require('../../models/kanji.model');
const hooks = require('./kanji.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/kanji', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('kanji');

  service.hooks(hooks);
};
