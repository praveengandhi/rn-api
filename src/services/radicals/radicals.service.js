// Initializes the `radicals` service on path `/radicals`
const createService = require('feathers-nedb');
const createModel = require('../../models/radicals.model');
const hooks = require('./radicals.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/radicals', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('radicals');

  service.hooks(hooks);
};
