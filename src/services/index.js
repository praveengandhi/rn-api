const users = require('./users/users.service.js');
const wordGroups = require('./word-groups/word-groups.service.js');
const words = require('./words/words.service.js');
const radicals = require('./radicals/radicals.service.js');
const kanjiGroups = require('./kanji-groups/kanji-groups.service.js');
const kanji = require('./kanji/kanji.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(users);
  app.configure(wordGroups);
  app.configure(words);
  app.configure(radicals);
  app.configure(kanjiGroups);
  app.configure(kanji);
};
