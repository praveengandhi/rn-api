// Initializes the `kanji-groups` service on path `/kanji-groups`
const createService = require('feathers-nedb');
const createModel = require('../../models/kanji-groups.model');
const hooks = require('./kanji-groups.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/kanji-groups', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('kanji-groups');

  service.hooks(hooks);
};
